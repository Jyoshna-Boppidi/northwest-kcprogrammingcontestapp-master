//
//  NewTeamViewController.swift
//  Northwest-KCProgrammingContestApp
//
//  Created by Boppidi,Jyoshna on 3/13/19.
//  Copyright © 2019 Boppidi,Jyoshna. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var STU0TF: UITextField!
    @IBOutlet weak var STU1TF: UITextField!
    @IBOutlet weak var STU2TF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
}
    var school: School!
    @IBAction func done(_ sender: Any) {
        let Name = nameTF.text!
        let Stu0 = STU0TF.text!
        let Stu1 = STU1TF.text!
        let Stu2 = STU2TF.text!
        school.addTeam(name: Name, students: [Stu0, Stu1, Stu2])
        self.dismiss(animated: true, completion: nil)
}
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
}
  
}
